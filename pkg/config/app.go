package config

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)


func Connect() *gorm.DB{
	// dsn := "akhil:akhil123@tcp(localhost:3306)/movies?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open("mysql" , "akhil:akhil123@/movies?charset=utf8mb4&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}

	return db
}

