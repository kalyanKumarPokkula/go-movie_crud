package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/kalyanKumarPokkula/movies/pkg/models"
	"github.com/kalyanKumarPokkula/movies/pkg/utils"
)

var NewMovie models.Movie

func GetMovies(w http.ResponseWriter , r *http.Request) {
	newMovies := models.GetAllMovies()
	res, _ := json.Marshal(newMovies)
	w.Header().Set("Content-Type" , "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}



func GetMovieById(w http.ResponseWriter , r *http.Request){
	params:= mux.Vars(r)
	movieId := params["movieId"]
	ID , err := strconv.ParseInt(movieId,0,0)
	if err != nil {
		fmt.Println("Error while parsing")
	}
	moviesDetalis , _ := models.GetMovieById(ID)
	res , _ := json.Marshal(moviesDetalis)
	w.Header().Set("Content-Type" , "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func CreateMovie(w http.ResponseWriter , r *http.Request){
	NewMovie := &models.Movie{}
	utils.ParseBody(r,NewMovie)
	fmt.Println(NewMovie)
	movie := models.CreateMovie(NewMovie)
	fmt.Println("successfully created a movie")
	res ,_ := json.Marshal(movie)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}


func DeleteMovieById(w http.ResponseWriter , r *http.Request){
	params := mux.Vars(r)
	movieId := params["movieId"]
	ID , err := strconv.ParseInt(movieId,0,0)
	if err != nil {
		fmt.Println("Error while parsing")
	}
	movie := models.DeleteMovieById(ID)
	res , _ := json.Marshal(movie)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}


func UpdateMovieById(w http.ResponseWriter , r *http.Request){
	var updateMovie = &models.Movie{}
	utils.ParseBody(r, updateMovie)
	params := mux.Vars(r)
	movieId := params["movieId"]
	ID , err := strconv.ParseInt(movieId,0,0)
	if err != nil {
		fmt.Println("Error while parsing")
	}
	movie ,db := models.GetMovieById(ID)
	if updateMovie.Title != "" {
		movie.Title = updateMovie.Title

	}
	if updateMovie.Description != ""{
		movie.Description = updateMovie.Description
	}
	db.Save(&movie)
	res, _ := json.Marshal(movie)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}