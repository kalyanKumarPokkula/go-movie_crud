package routes

import (
	"github.com/gorilla/mux"
	"github.com/kalyanKumarPokkula/movies/pkg/controllers"
)


var MovieRoutes = func(router *mux.Router){
	router.HandleFunc("/movies" , controllers.CreateMovie).Methods("POST")
	router.HandleFunc("/movies", controllers.GetMovies).Methods("GET")
	router.HandleFunc("/movies/{movieId}", controllers.GetMovieById).Methods("GET")
	router.HandleFunc("/movies/{movieId}", controllers.UpdateMovieById).Methods("PUT")
	router.HandleFunc("/movies/{movieId}", controllers.DeleteMovieById).Methods("DELETE")
}
