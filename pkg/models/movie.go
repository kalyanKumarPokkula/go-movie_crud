package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"github.com/kalyanKumarPokkula/movies/pkg/config"
)

var db *gorm.DB

type Movie struct{
	gorm.Model
	Title string 
	Description string

}

func init(){
	db := config.Connect()
	db.AutoMigrate(&Movie{})
}

func CreateMovie(m *Movie) *Movie{
	fmt.Println(m)
	newMovie := Movie{
		Title: "New Movie",
		Description:  "This is Movie Description",
	}
	db.Create(&newMovie)
	fmt.Println("Movie created successfully")
	return &newMovie
	
}

func GetAllMovies() []Movie{
	var Movies []Movie
	db.Find(&Movies)
	return Movies
}

func GetMovieById(Id int64) (*Movie, *gorm.DB){
	var getMovie Movie
	db := db.Where("ID=?", Id).Find(&getMovie)
	return &getMovie , db
}

func DeleteMovieById(Id int64) Movie{
	var movie Movie
	db.Where("ID=?" ,Id).Delete(&movie)
	return movie
}