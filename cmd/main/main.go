package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/kalyanKumarPokkula/movies/pkg/routes"
)

func main() {
	r := mux.NewRouter()
	routes.MovieRoutes(r)
	http.Handle("/" ,r)
	fmt.Printf("Starting server at port 8080\n")
	log.Fatal(http.ListenAndServe("localhost:8080" , r))
	
}